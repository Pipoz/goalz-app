package com.example.goalz;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface GoalDao {
    @Insert
    void insert(Goal goal);

    @Query("DELETE FROM goal_table")
    void deleteAll();

    @Query("SELECT * from goal_table ORDER BY name ASC")
    LiveData<List<Goal>> getAllGoals();

    @Query("SELECT * from goal_table WHERE id = :id")
    Goal getGoal(int id);

    @Query("DELETE FROM goal_table WHERE id = :id")
    void deleteGoal(int id);

    @Query("UPDATE goal_table SET name = :name, description = :description, checked = :c WHERE id = :id")
    void updateGoal(int id, String name, String description, boolean c);

}
