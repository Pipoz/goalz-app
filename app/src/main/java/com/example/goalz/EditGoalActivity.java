package com.example.goalz;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EditGoalActivity extends AppCompatActivity {

    public static final String NEW_GOAL = "com.example.android.wordlistsql.NEW_GOAL";
    public static final String DESCRIPTION = "com.example.android.wordlistsql.DESCRIPTION";
    public static final String DELETE = "com.example.android.wordlistsql.DELETE";
    public static final String CHECK = "com.example.android.wordlistsql.CHECK";
    public static final String ID = "com.example.android.wordlistsql.ID";
    private static final int RESULT_SAVE = 3 ;
    private static final int RESULT_DELETE = 4 ;
    private static final int RESULT_CHECK = 5 ;

    private EditText mEditNameView;
    private EditText mEditDescView;
    private TextView mTextEditGoal;
    private Goal editedGoal;
    private Button buttonCheck;
    private Button buttonSave;
    private Button buttonDelete;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        //We get the edited goal here
        editedGoal = (Goal) intent.getSerializableExtra(MainActivity.EDITED_GOAL);

        setContentView(R.layout.activity_edit_goal);
        mEditNameView = findViewById(R.id.goal_name_edit);
        mEditDescView= findViewById(R.id.goal_description_edit);
        mTextEditGoal = findViewById(R.id.edit_goal_title);
        buttonCheck = findViewById(R.id.button_check_goal);
        buttonSave = findViewById(R.id.button_save_goal);
        buttonDelete = findViewById(R.id.button_delete_goal);

        //get the goal infos and display it into corresponding fields
        mEditNameView.setText(editedGoal.getName());
        mEditDescView.setText(editedGoal.getDescription());
        mTextEditGoal.setText("Edit Goal #" + editedGoal.getId());

        if(editedGoal.isChecked()){
            buttonCheck.setText("Uncheck");
            buttonCheck.setBackgroundColor(getResources().getColor(R.color.colorDanger));
        }else {
            buttonCheck.setText("Achieved");
            buttonCheck.setBackgroundColor(getResources().getColor(R.color.colorSuccess));
        }


        //Button Save
        buttonSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mEditNameView.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String name = mEditNameView.getText().toString();
                    String description = mEditDescView.getText().toString();
                    editedGoal.setName(name);
                    editedGoal.setDescription(description);
                    replyIntent.putExtra(NEW_GOAL, editedGoal);
                    replyIntent.putExtra(ID, editedGoal.getId());

                    setResult(RESULT_SAVE , replyIntent);
                }
                finish();
            }
        });

        //Button Delete
        buttonDelete.setOnClickListener(new View.OnClickListener() {


            public void onClick(View view) {
                Intent replyIntent = new Intent();
                //TODO : Display a "do you really want to delete ?" popup
                replyIntent.putExtra(ID, editedGoal.getId());

                setResult(RESULT_DELETE, replyIntent);

                finish();
            }
        });

        //Button Check
        buttonCheck.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                // CHECK the goal
                if(editedGoal.isChecked()){
                    editedGoal.setChecked(false);
                }else{
                    editedGoal.setChecked(true);
                }
                replyIntent.putExtra(ID, editedGoal.getId());
                replyIntent.putExtra(NEW_GOAL, editedGoal);
                setResult(RESULT_CHECK , replyIntent);
                finish();
            }
        });
    }
}
