package com.example.goalz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewGoalActivity extends AppCompatActivity {

    public static final String NAME = "com.example.android.wordlistsql.NAME";
    public static final String DESCRIPTION = "com.example.android.wordlistsql.DESCRIPTION";

    private EditText mEditNameView;
    private EditText mEditDescView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_goal);
        mEditNameView = findViewById(R.id.edit_name);
        mEditDescView= findViewById(R.id.edit_description);

        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mEditNameView.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String name = mEditNameView.getText().toString();
                    String description = mEditDescView.getText().toString();
                    replyIntent.putExtra(NAME, name);
                    replyIntent.putExtra(DESCRIPTION, description);

                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });
    }
}
