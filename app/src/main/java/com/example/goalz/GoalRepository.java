package com.example.goalz;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class GoalRepository {
    private GoalDao mGoalDao;
    private LiveData<List<Goal>> mAllGoals;

    GoalRepository(Application application) {
        GoalRoomDatabase db = GoalRoomDatabase.getDatabase(application);
        mGoalDao = db.goalDao();
        mAllGoals = mGoalDao.getAllGoals();
    }

    LiveData<List<Goal>> getAllGoals() {
        return mAllGoals;
    }

    public void deleteGoal(int id){
        Goal goalDeleted = null;
        for(Goal g : mAllGoals.getValue()){
            if(g.getId() == id){
                goalDeleted = g;
            }
        }
        try{
            new deleteAsyncTask(mGoalDao).execute(goalDeleted);
        }catch (Error e){
            Log.e("UPDATE ERROR",
                    "THE GOAL that we are trying to suppress is not in the mAllGoal liveData list");
        }
    }

    public void updateGoal(int id, Goal newGoal){
        newGoal.setId(id);
        try{
          new updateAsyncTask(mGoalDao).execute(newGoal);
        }catch (Error e){
            Log.e("UPDATE ERROR",
                    "THE GOAL that we are trying to update is not in the mAllGoal liveData list");
        }
    }

    public void insert (Goal goal) {
        new insertAsyncTask(mGoalDao).execute(goal);
    }

    private static class insertAsyncTask extends AsyncTask<Goal, Void, Void> {

        private GoalDao mAsyncTaskDao;

        insertAsyncTask(GoalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Goal... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }

    }
    private static class updateAsyncTask extends AsyncTask<Goal, Void, Void> {

        private GoalDao mAsyncTaskDao;

        updateAsyncTask(GoalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Goal... params) {
            mAsyncTaskDao.updateGoal(params[0].getId(), params[0].getName(), params[0].getDescription(),
                    params[0].isChecked());
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Goal, Void, Void> {

        private GoalDao mAsyncTaskDao;

        deleteAsyncTask(GoalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Goal... params) {
            mAsyncTaskDao.deleteGoal(params[0].getId());
            return null;
        }
    }
}
