package com.example.goalz;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

public class GoalListAdapter extends RecyclerView.Adapter<GoalListAdapter.GoalViewHolder> {

    class GoalViewHolder extends RecyclerView.ViewHolder {
        private final TextView goalName;
        private final TextView goalDescription;
        private final CheckBox goalChecked;

        private GoalViewHolder(View itemView) {
            super(itemView);
            goalName = itemView.findViewById(R.id.goal_name);
            goalDescription = itemView.findViewById(R.id.goal_description);
            goalChecked = itemView.findViewById(R.id.goal_checkbox);
        }
    }

    private final LayoutInflater mInflater;
    private List<Goal> mGoals; // Cached copy of goals

    GoalListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public GoalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.goal_layout, parent, false);
        return new GoalViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GoalViewHolder holder, int position) {
        if (mGoals != null) {
            Goal current = mGoals.get(position);
            holder.goalName.setText(current.getName());
            holder.goalDescription.setText(current.getDescription());
            if(current.isChecked()){
                holder.goalChecked.setChecked(true);
            }else {
                holder.goalChecked.setChecked(false  );
            }
        } else {
            // Covers the case of data not being ready yet.
            holder.goalName.setText("No Goal");
        }
    }

    void setGoals(List<Goal> goals){
        mGoals = goals;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mGoals has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mGoals != null)
            return mGoals.size();
        else return 0;
    }

    /*
     * Return the Goal corresponding to a position in the list.
     */
    public Goal getGoal(int position){
        return this.mGoals.get(position);
    }
}
