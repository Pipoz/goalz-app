package com.example.goalz;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

public class GoalViewModel extends AndroidViewModel {
    private GoalRepository mRepository;

    private LiveData<List<Goal>> mAllGoals;

    public GoalViewModel (Application application) {
        super(application);
        mRepository = new GoalRepository(application);
        mAllGoals = mRepository.getAllGoals();
    }

    LiveData<List<Goal>> getAllGoals() { return mAllGoals; }

    public void deleteGoal(int id){
        mRepository.deleteGoal(id);
    }

    public void insert(Goal goal) { mRepository.insert(goal); }

    public void updateGoal(int id, Goal newGoal){
        mRepository.updateGoal(id, newGoal);
    }
}
