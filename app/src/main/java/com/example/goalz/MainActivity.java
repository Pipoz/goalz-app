package com.example.goalz;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.goalz.Utils.ItemClickSupport;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int NEW_GOAL_ACTIVITY_REQUEST_CODE = 1;
    private static final int EDIT_GOAL_ACTIVITY_REQUEST_CODE = 2;
    public static final String EDITED_GOAL = "com.example.android.wordlistsql.EDITED_GOAL";
    private static final int RESULT_SAVE = 3 ;
    private static final int RESULT_DELETE = 4 ;
    private static final int RESULT_CHECK = 5 ;
    private GoalViewModel mGoalViewModel;
    private RecyclerView recyclerView;
    private GoalListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navbar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.recyclerview);
        adapter = new GoalListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mGoalViewModel = ViewModelProviders.of(this).get(GoalViewModel.class);

        mGoalViewModel.getAllGoals().observe(this, new Observer<List<Goal>>() {
            @Override
            public void onChanged(@Nullable final List<Goal> goals) {
                // Update the cached copy of the goals in the adapter.
                adapter.setGoals(goals);
            }
        });

        //Configure the click on a goal
        this.configureOnClickRecyclerView();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewGoalActivity.class);
                startActivityForResult(intent, NEW_GOAL_ACTIVITY_REQUEST_CODE);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void configureOnClickRecyclerView() {
        ItemClickSupport.addTo(recyclerView, R.layout.goal_layout)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        Log.e("TAG", "Position : "+position);
                        Goal goalClicked = adapter.getGoal(position);
                        Intent intent = new Intent(MainActivity.this, EditGoalActivity.class);
                        intent.putExtra(EDITED_GOAL, goalClicked);
                        startActivityForResult(intent, EDIT_GOAL_ACTIVITY_REQUEST_CODE);
                    }
                });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_goals) {
            // Handle the camera action
        } else if (id == R.id.nav_calendar) {

        } else if (id == R.id.nav_achieved) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_profile) {

        } else if (id == R.id.nav_disconnect) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_GOAL_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {

            Goal goal = new Goal(data.getStringExtra(NewGoalActivity.NAME));
            goal.setDescription(data.getStringExtra(NewGoalActivity.DESCRIPTION));
            mGoalViewModel.insert(goal);

        }else if(requestCode == EDIT_GOAL_ACTIVITY_REQUEST_CODE && resultCode == RESULT_DELETE){

            mGoalViewModel.deleteGoal(data.getIntExtra(EditGoalActivity.ID, -1));

        }else if(requestCode == EDIT_GOAL_ACTIVITY_REQUEST_CODE && resultCode == RESULT_CHECK){

            //CHECK the goal
            int newId = data.getIntExtra(EditGoalActivity.ID,-1);
            Goal goal = (Goal) data.getSerializableExtra(EditGoalActivity.NEW_GOAL);
            mGoalViewModel.updateGoal(newId, goal);

        }else if(requestCode == EDIT_GOAL_ACTIVITY_REQUEST_CODE && resultCode == RESULT_SAVE){

            int newId = data.getIntExtra(EditGoalActivity.ID,-1);
            Goal goal = (Goal) data.getSerializableExtra(EditGoalActivity.NEW_GOAL);
            mGoalViewModel.updateGoal(newId, goal);

        } else if (requestCode == NEW_GOAL_ACTIVITY_REQUEST_CODE) {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }
}
